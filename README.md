# Awesome open source employers

This is a curated list of awesome employers with a strong focus on open source.
Furthermore, a company must be awesome, which excludes companies
* developing / hosting environmentally unsustainable on our one and only planet,
* spying on their customers or users, (e.g. Facebook or Microsoft)
* undercutting workers' rights or preventing the formation of unions, (e.g. Amazon)
* extending open formats with proprietary additions with no reasonable fallback for other clients.

For now, the list is sorted alphabetically.

## FOSSA

The company does compliance checking when using open source code.
Their product discourage the use/inclusion of viral licenses like GPL.
They are based in San Francisco, CA, USA.

https://fossa.com/careers/

## KIProtect

A very small start up in Berlin, Germany. They do data projection through pseudonymization.
Publishes and maintains data analysis libraries and React components open source.
In the internal selection of software, open source and self-hosted solutions were favoured.

https://kiprotect.com/company/career

## Mattermost

They develop a platform for team communication (alternative to Slack).
Mattermost has remote jobs and jobs in offices located in North America (e.g. Austin, TX; Seattle, WA; Toronto, Canada) and Europe (e.g. Berlin).

https://mattermost.com/careers/

## Mozilla

The company behind the Firefox browser and a big advocate for Internet Health.
Most teams work remotely across timezones and the company has many offices all over the world. Their head quarter is in Mountain View, CA, USA.
Mozilla has a big community which gets favored in the hiring process.
Publishes and maintains a lot of open source but uses lots of proprietary software internally.

https://careers.mozilla.org/

## Element

The company behind the decentralized messaging protocol Matrix.org and the Element chat client.
They are based in the UK with the option to work remote for some positions.
In 2018 the French government decided to prepare a roll out of Matrix to 5 million civil servants ([announcement](https://matrix.org/blog/2018/04/26/matrix-and-riot-confirmed-as-the-basis-for-frances-secure-instant-messenger-app)).

* [Manifesto on Google Docs](https://docs.google.com/document/d/1-nIVrMTyiCp1yLIrL62xFNtyJ91SY17LRSTr1_VByno/edit)

https://element.io/careers

## Suse

Multinational German-based company behind the Suse and Open Suse Linux distribution.
They have offices in Germany, the US and other places.
Suse is involved in a lot of important open source projects, mainly regarding Linux.

https://www.suse.com/company/careers
